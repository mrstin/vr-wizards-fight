﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppContext {

    public static WebManager WebManager { get; set; }
    public static UriManager Uri { get; set; }
    //public static DialogManager DialogManager { get; set; }
    //public static string CurrentUri { get; set; }
    public static JsonSerialize Json { get; set; }

    public static void Configure()
    {
        Uri = new UriManager();
        Json = new JsonSerialize();
        //WebManager = new WebManager();
        //DialogManager = new DialogManager();
    }

	public static Vector3 enemyPosition;
    public static Vector3 currentPosition;
}
