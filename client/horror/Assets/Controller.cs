﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	public Transform frozen;
    public Transform shadow;
    public Transform circle;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("a")||Input.GetKey(KeyCode.JoystickButton1)) {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            bool isHit = Physics.Raycast(_ray, out _hit, 1000);
            Debug.Log("first");
            if (isHit && _hit.transform.name == "Player1")
            {
                Transform m = Instantiate(frozen, AppContext.enemyPosition, Quaternion.identity) as Transform;
                //SendDamage(0);
            }
		}
        if (Input.GetKeyDown("s") || Input.GetKey(KeyCode.JoystickButton2))
        {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            bool isHit = Physics.Raycast(_ray, out _hit, 1000);
            Debug.Log("second");
            if (isHit && _hit.transform.name == "Player1")
            {
                Transform m = Instantiate(shadow, AppContext.enemyPosition, Quaternion.identity) as Transform;
                //SendDamage(1);
            }
        }
        if (Input.GetKeyDown("d") || Input.GetKey(KeyCode.JoystickButton3))
        {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            bool isHit = Physics.Raycast(_ray, out _hit, 1000);
            Debug.Log("third");
            if (isHit && _hit.transform.name == "Player1")
            {
                Transform m = Instantiate(circle, AppContext.enemyPosition, Quaternion.identity) as Transform;
                //SendDamage(2);
            }
        }
    }

	private async void SendDamage(int i){
		await AppContext.WebManager.SendMessage (new Damage (i));
	}
}
