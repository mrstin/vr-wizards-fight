﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static GameObject Load(string name){
		return Instantiate (Resources.Load (name, typeof(GameObject)), AppContext.currentPosition, Quaternion.identity) as GameObject;
	}

    /*public static GameObject Load1(string name)
    {
        return Instantiate(Resources.Load(name, typeof(GameObject)), new Vector3(AppContext.currentPosition.x, AppContext.currentPosition.y, AppContext.currentPosition.z+40), Quaternion.identity) as GameObject;
    }*/
}	
