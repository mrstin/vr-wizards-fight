﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCast : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        #region
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit _hit;
            Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            bool isHit = Physics.Raycast(_ray, out _hit, 1000);
            if (isHit)
            {
                Debug.Log(_hit.transform.name);
                Debug.Log(_hit.distance);
            }
            GameObject _lineObject = new GameObject();
            LineRenderer _line = _lineObject.AddComponent<LineRenderer>();
            _line.SetPosition(0, _ray.origin);
            if (isHit)
            {
                _line.SetPosition(1, _hit.point);
            }else
            {
                _line.SetPosition(1, _ray.direction);
            }
            _line.SetWidth(0.05f, 0.05f);
            Destroy(_lineObject, 0.05f);
        }
        #endregion

        #region
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit[] _hitArray;
            Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            _hitArray = Physics.RaycastAll(_ray,  100);
            if (_hitArray.Length!=0)
            {                
                Debug.Log("Hits object: ");
                foreach(var _hit in _hitArray)
                {
                    Debug.Log(_hit.transform.name); 
                }
                Debug.Log("End");
            }          
        }
        #endregion
    }
}



