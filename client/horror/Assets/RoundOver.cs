﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RoundOver {

    public string type = "roundOver";
    public bool result;
}
