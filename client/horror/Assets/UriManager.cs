﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class UriManager  {

    public string GetUri()
    {
		using (StreamReader sr = new StreamReader(System.Net.WebRequest.Create("http://sechoton-ipru.000webhostapp.com/getip.php").GetResponse().GetResponseStream()))
        {
            string response = sr.ReadToEnd();
            return "ws://" + response;
        }

    }
}
